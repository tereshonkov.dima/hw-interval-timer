// Практичне завдання 1:


// -Створіть HTML-файл із кнопкою та елементом div.

// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

const start = document.getElementById("start-text");
const text = document.getElementById("text");

start.addEventListener("click", function () { 
    setTimeout (() => {
        text.textContent = "Операція виконана успішно"
    }, 3000)
});

// Практичне завдання 2:


// Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".

const startTimer = document.getElementById("start-timer");

function startInterval () {
    let time = 10;
    let timer = document.getElementById("timer");
    timer.textContent = time;
    timer.style.border = "1px solid red"
    const intervalId = setInterval(() => {
        time--;
        timer.textContent = time;
        if (time <= 0) {
            clearInterval(intervalId);
            timer.textContent = "Зворотній відлік завершено";
            timer.style.border = "none";
        } 
    }, 1000)
};
startTimer.addEventListener("click", startInterval);